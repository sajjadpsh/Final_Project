package ir.ac.aut.logic.massage;

import ir.ac.aut.logic.network.BaseMassage;
import ir.ac.aut.logic.network.MessageTypes;

import java.nio.ByteBuffer;

public class PlayerReady extends BaseMassage {

    private String mReadiness;

    public PlayerReady(String readiness)
    {
        mReadiness = readiness;
        serialize();
    }

    public PlayerReady(byte[] serialized)
    {
        mSerialized= serialized;
        deserialize();
    }


    @Override
    protected void serialize() {
        int readyLength = mReadiness.getBytes().length;
        int messageLength = 4+1+1+4+readyLength;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.PLAYER_READY);
        byteBuffer.putInt(readyLength);
        byteBuffer.put(mReadiness.getBytes());
        mSerialized = byteBuffer.array();
    }

    @Override
    protected void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        byte messageType = byteBuffer.get();
        int readyLength = byteBuffer.getInt();
        byte[] readyBytes = new byte[readyLength];
        byteBuffer.get(readyBytes);
        mReadiness = new String(readyBytes);
    }

    @Override
    public Byte getMassageType() {
        return MessageTypes.PLAYER_READY;
    }
}

