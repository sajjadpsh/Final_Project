package ir.ac.aut.logic.massage;

import ir.ac.aut.logic.network.BaseMassage;
import ir.ac.aut.logic.network.MessageTypes;

import java.nio.ByteBuffer;

public class PlayerAbort extends BaseMassage {

    private String mAbborted;

    public PlayerAbort(String abborted)
    {
        mAbborted = abborted;
        serialize();
    }

    public PlayerAbort(byte[] serialized)
    {
        mSerialized = serialized;
        deserialize();
    }

    @Override
    protected void serialize() {
        int abbortLength = mAbborted.getBytes().length;
        int messageLength = 4+1+1+4+abbortLength;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.PLAYER_ABBORT);
        byteBuffer.putInt(abbortLength);
        byteBuffer.put(mAbborted.getBytes());
        mSerialized = byteBuffer.array();
    }

    @Override
    protected void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        byte messageType = byteBuffer.get();
        int abbortLength = byteBuffer.getInt();
        byte[] abbortBytes = new byte[abbortLength];
        byteBuffer.get(abbortBytes);
        mAbborted = new String(abbortBytes);
    }

    @Override
    public Byte getMassageType() {
        return MessageTypes.PLAYER_ABBORT;
    }
}
