package ir.ac.aut.logic.massage;

import ir.ac.aut.logic.network.BaseMassage;
import ir.ac.aut.logic.network.MessageTypes;

import java.nio.ByteBuffer;

public class MoveResult extends BaseMassage {

    private String mResult;

    private String mShipKind;

    public MoveResult(String result , int shipKind) {
        mResult = result;
        mShipKind = Integer.toString(shipKind);
        serialize();

    }

    public MoveResult(byte[] serialized)
    {
        mSerialized = serialized;
        deserialize();
    }


    @Override
    protected void serialize() {
        int resultLength = mResult.getBytes().length;
        int shipKindLength = mShipKind.getBytes().length;
        int messageLength = 4+1+1+4+resultLength+4+shipKindLength;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.PLAYER_MOVE_RESULT);
        byteBuffer.putInt(resultLength);
        byteBuffer.put(mResult.getBytes());
        byteBuffer.putInt(shipKindLength);
        byteBuffer.put(mShipKind.getBytes());
        mSerialized = byteBuffer.array();
    }

    @Override
    protected void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        byte messageType = byteBuffer.get();
        int resultLength = byteBuffer.getInt();
        byte[] resultBytes = new byte[resultLength];
        byteBuffer.get(resultBytes);
        mResult = new String(resultBytes);
        int shipKindLength = byteBuffer.getInt();
        byte[] shipKindBytes = new byte[shipKindLength];
        byteBuffer.get(shipKindBytes);
        mShipKind = new String(shipKindBytes);
    }

    @Override
    public Byte getMassageType() {
        return MessageTypes.PLAYER_MOVE_RESULT;
    }
}

