package ir.ac.aut.logic.massage;

import ir.ac.aut.logic.network.BaseMassage;
import ir.ac.aut.logic.network.MessageTypes;

import java.nio.ByteBuffer;

/**
 * Created by Sajjad on 7/9/2017.
 */
public class MoveHit extends BaseMassage {

    private String mButtonNumber;

    public MoveHit(int buttonNumber)
    {
        mButtonNumber = Integer.toString(buttonNumber);
        serialize();
    }

    public MoveHit(byte[] serialized)
    {
        mSerialized = serialized;
        deserialize();
    }

    @Override
    protected void serialize() {

        int chatLength = mButtonNumber.getBytes().length;
        int messageLength = 4+1+1+4+chatLength;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.PLAYER_MOVE_HEAT);
        byteBuffer.putInt(chatLength);
        byteBuffer.put(mButtonNumber.getBytes());
        mSerialized = byteBuffer.array();
    }

    @Override
    protected void deserialize() {

        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        byte messageType = byteBuffer.get();
        int buttonLength = byteBuffer.getInt();
        byte[] buttonBytes = new byte[buttonLength];
        byteBuffer.get(buttonBytes);
        mButtonNumber = new String(buttonBytes);
    }

    @Override
    public Byte getMassageType() {
        return MessageTypes.PLAYER_MOVE_HEAT;
    }
}

