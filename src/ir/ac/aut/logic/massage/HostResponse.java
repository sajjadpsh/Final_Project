package ir.ac.aut.logic.massage;

/**
 * Created by Sajjad on 7/9/2017.
 */


import ir.ac.aut.logic.network.BaseMassage;
import ir.ac.aut.logic.network.MessageTypes;

import java.nio.ByteBuffer;

public class HostResponse extends BaseMassage {

    private String mResponse;

    public HostResponse(String response)
    {
        mResponse = response;
        serialize();
    }

    public HostResponse(byte[] serialized)
    {
        mSerialized = serialized;
        deserialize();
    }

    @Override
    protected void serialize() {
        int respnseLength = mResponse.getBytes().length;
        int messageLength = 4+1+1+4+respnseLength;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.HOST_RESPONSE);
        byteBuffer.putInt(respnseLength);
        byteBuffer.put(mResponse.getBytes());
        mSerialized = byteBuffer.array();

    }

    @Override
    protected void deserialize() {

        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        byte messageType = byteBuffer.get();
        int responseLength = byteBuffer.getInt();
        byte[] responseBytes = new byte[responseLength];
        byteBuffer.get(responseBytes);
        mResponse = new String(responseBytes);
    }


    @Override
    public Byte getMassageType() {
        return MessageTypes.HOST_RESPONSE;
    }

}
