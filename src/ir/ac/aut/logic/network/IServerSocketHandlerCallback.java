package ir.ac.aut.logic.network;

/**
 * Created by Sajjad on 7/3/2017.
 */
public interface IServerSocketHandlerCallback {
    void onNewConnectionReceived(NetworkHandler networkHandler);
}