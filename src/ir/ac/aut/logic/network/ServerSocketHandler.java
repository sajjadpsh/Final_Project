package ir.ac.aut.logic.network;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Sajjad on 7/3/2017.
 */
public class ServerSocketHandler extends Thread {

    private TcpChannel tcpChannel;
    private ServerSocket mServerSocket;
    private Socket mSocket;
    private INetworkHandlerCallback mINetworkHandlerCallBack;
    private IServerSocketHandlerCallback mIServerSocketHandlerCallBack;

    public ServerSocketHandler(int port, INetworkHandlerCallback iNetworkHandlerCallBack, IServerSocketHandlerCallback iServerSocketHandlerCallBack) throws IOException {
        mServerSocket = new ServerSocket(port);
        mINetworkHandlerCallBack = iNetworkHandlerCallBack;
        mIServerSocketHandlerCallBack = iServerSocketHandlerCallBack;
    }

    @Override
    public void run(){
        while (!mServerSocket.isClosed() && Thread.currentThread().isAlive()){
            try {
                mSocket = mServerSocket.accept();
                mIServerSocketHandlerCallBack.onNewConnectionReceived(new NetworkHandler(mSocket, mINetworkHandlerCallBack));
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                this.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * kill the thread and close the server socket
     */
    public void stopSlef() throws IOException {
        mServerSocket.close();
        this.interrupt();
    }
}
