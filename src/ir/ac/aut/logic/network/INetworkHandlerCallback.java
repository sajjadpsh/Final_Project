package ir.ac.aut.logic.network;

import java.io.IOException;

/**
 * Created by Sajjad on 7/3/2017.
 */
public interface INetworkHandlerCallback {
    void onMessageReceived(BaseMassage baseMessage);
    void onSocketClosed() throws IOException;
}
