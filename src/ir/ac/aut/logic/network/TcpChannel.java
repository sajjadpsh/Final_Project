package ir.ac.aut.logic.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;

/**
 * Created by Sajjad on 7/3/2017.
 */
public class TcpChannel {

        private Socket mSocket;
        private OutputStream mOutputStream;
        private InputStream mInputStream;

    public TcpChannel(SocketAddress socketAddress, int timeout) throws IOException {

        mSocket=new Socket();
        mSocket.connect(socketAddress);
        mSocket.setSoTimeout(timeout);
        mInputStream = mSocket.getInputStream();
        mOutputStream = mSocket.getOutputStream();
    }

    public TcpChannel(Socket socket, int timeout) throws IOException {

        this.mSocket=socket;
        mSocket.setSoTimeout(timeout);
        mInputStream = mSocket.getInputStream();
        mOutputStream = mSocket.getOutputStream();
    }

    /**
     * Try to read specific count from input stream.
     */
    public byte[] read(final int count) throws IOException {

        byte[] data = new byte[count];
        mInputStream.read(data);
        return data;
    }

    /**
     * Write bytes on output stream.
     */
    public void write(byte[] data) throws IOException {

        mOutputStream.write(data);
        mOutputStream.flush();
    }

    /**
     * Check socket’s connectivity.
     */
    public boolean isConnected() {
        return mSocket.isConnected()&&!mSocket.isClosed();
    }

    /**
     * Try to close socket and input-output streams.
     */
    public void closeChannel() throws IOException {
        mSocket.close();
        mInputStream.close();
        mOutputStream.close();
    }
}

