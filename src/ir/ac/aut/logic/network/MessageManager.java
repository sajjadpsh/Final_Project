package ir.ac.aut.logic.network;

import ir.ac.aut.logic.massage.Chat;
import ir.ac.aut.logic.massage.LoginClient;
import ir.ac.aut.logic.massage.LoginHost;

import javax.swing.*;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.List;

public class MessageManager implements INetworkHandlerCallback,IServerSocketHandlerCallback {

    private ServerSocketHandler mServerSocketHandler;
    private List<NetworkHandler> mNetworkHandlerList;
    NetworkHandler mNetworkHandler;
    String mName;
    JTextArea mJtextArea;

    public MessageManager(int port , JTextArea jTextArea) throws IOException {
        mJtextArea = jTextArea;
        mServerSocketHandler = new ServerSocketHandler(port,this, this);
        mServerSocketHandler.start();
    }

    public MessageManager(String ip, int port , JTextArea jTextArea) throws IOException {
        mJtextArea = jTextArea;
        SocketAddress socketAddress = new InetSocketAddress(ip, port);
        mNetworkHandler = new NetworkHandler(socketAddress, this);
        mNetworkHandler.start();
    }


    @Override
    public void onNewConnectionReceived(NetworkHandler networkHandler) {
        mNetworkHandler = networkHandler;
        mNetworkHandler.start();
    }

    @Override
    public void onMessageReceived(BaseMassage baseMessage) {
        System.out.println("1a");
        switch (baseMessage.getMassageType()) {
            case MessageTypes.REQUEST_LOGIN_CLIENT:
                System.out.println("1b");
                consumeRequestLoginClient((LoginClient) baseMessage);
                break;
            case MessageTypes.REQUEST_LOGIN_HOST:
                System.out.println("1c");
                System.out.println("lOLOLLLL");
                consumeRequestLoginHost((LoginHost) baseMessage);
                break;
            case MessageTypes.PLAYER_CHAT:
                System.out.println("1d");
                consumePlayerChatMSG((Chat) baseMessage);
                break;

        }
    }


    @Override
    public void onSocketClosed() throws IOException {
        mServerSocketHandler.stopSlef();
    }

    private void consumeRequestLoginClient(LoginClient loginClient) {
        mName = loginClient.getPassword();
        System.out.println("asasaass              " + mName);
    }

    private void consumeRequestLoginHost(LoginHost requestLoginClientMSG) {
        mName = requestLoginClientMSG.getPassword();
        System.out.println("asasaass              " + mName);
    }

    private void consumePlayerChatMSG(Chat chat)
    {
        System.out.println("ddddddddddddddddddddd"+ chat.getmChat());
        mJtextArea.append(mName+">>>" + chat.getmChat());
    }

    public void sendRequestLoginclientMSG(String string1, String string2, String string3) throws IOException {
        LoginClient loginClient = new LoginClient(string1, string2, string3);
        mNetworkHandler.sendMessage(loginClient);
    }

    public void sendRequestLoginHostMSG(String string1 , int string2) throws IOException {
        LoginHost loginHost = new LoginHost(string1 , string2);
        mNetworkHandler.sendMessage(loginHost);
    }

    public void sendPlayerChatMSG(String string) throws IOException {
        Chat chat = new Chat(string);
        mNetworkHandler.sendMessage(chat);
    }

    public String getmName()
    {
        return mName;
    }
}