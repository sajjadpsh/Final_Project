package ir.ac.aut.logic.network;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.Queue;

public class NetworkHandler extends Thread {
    private TcpChannel mTcpChannel;
    private Queue<byte[]> mSendQueue;
    private Queue<byte[]> mReceivedQueue;
    private ReceiveMessageConsumer mConsumerThread;
    private INetworkHandlerCallback mINetworkHandlerCallBack;

    public NetworkHandler(SocketAddress socketAddress, INetworkHandlerCallback iNetworkHandlerCallback) throws IOException {
        mTcpChannel = new TcpChannel(socketAddress, 3000);
        mINetworkHandlerCallBack = iNetworkHandlerCallback;
        mSendQueue = new LinkedList<byte[]>();
        mReceivedQueue = new LinkedList<byte[]>();
        mSendQueue.clear();
        mReceivedQueue.clear();
        mConsumerThread = new ReceiveMessageConsumer();
        mConsumerThread.start();

    }

    public NetworkHandler(Socket socket, INetworkHandlerCallback iNetworkHandlerCallBack) throws IOException {
        mTcpChannel = new TcpChannel(socket, 300);
        mINetworkHandlerCallBack = iNetworkHandlerCallBack;
        mSendQueue = new LinkedList<byte[]>();
        mReceivedQueue = new LinkedList<byte[]>();
        mSendQueue.clear();
        mReceivedQueue.clear();
        mConsumerThread = new ReceiveMessageConsumer();
        mConsumerThread.start();
    }

    public void sendMessage(BaseMassage baseMessage) throws IOException {
        mSendQueue.add(baseMessage.getSerialized());
    }

    @Override
    public void run() {
        while (this.isAlive() && mTcpChannel.isConnected()) {
            if (!mSendQueue.isEmpty())
                try {
                    mTcpChannel.write(mSendQueue.poll());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            else {
                try {
                    byte[] readData = this.readChannel();
                    if (readData != null)
                        mReceivedQueue.add(readData);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stopSelf() throws IOException {
        mTcpChannel.closeChannel();
        this.interrupt();
    }

    private byte[] readChannel() throws IOException {
        byte[] messageLengthByte = mTcpChannel.read(4);
        if (messageLengthByte == null)
            return null;
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.put(messageLengthByte);
        int messageLength = byteBuffer.getInt(0);
        return mTcpChannel.read(messageLength - 4);
    }

    private class ReceiveMessageConsumer extends Thread {

        @Override
        public void run() {
            while (this.isAlive() && mTcpChannel.isConnected()) {
                if (!mReceivedQueue.isEmpty()) {
                    byte[] receivedData = mReceivedQueue.poll();
                    BaseMassage message;

                    switch (receivedData[2]) {
                        case MessageTypes.REQUEST_LOGIN_HOST:
                            message = new RequestLoginMessage(receivedData);
                            break;

                        default:
                            message = new RequestLoginMessage(receivedData);
                            break;
                    }

                    mINetworkHandlerCallBack.onMessageReceived(message);
                } else
                    try {
                        this.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
            }
        }
    }
}