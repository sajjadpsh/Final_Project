package ir.ac.aut.logic.network;

import java.nio.ByteBuffer;

public class RequestLoginMessage extends BaseMassage {
    private String mUsername;
    private String mIP;
    private int mPort;

    public RequestLoginMessage(String username, String ip, int port) {
        mUsername = username;
        mIP = ip;
        mPort = port;
        serialize();
    }

    public RequestLoginMessage(byte[] serialized) {
        mSerialized = serialized;
        deserialize();
    }

    @Override
    protected void serialize() {
        int usernameLength = mUsername.length();
        int ipLength = mIP.length();
        int messageLength = 4 + 1 + 1 + 4 + usernameLength + 4 + ipLength + 4;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put((byte) MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put((byte) MessageTypes.REQUEST_LOGIN_HOST);
        byteBuffer.putInt(usernameLength);
        byteBuffer.put(mUsername.getBytes());
        byteBuffer.putInt(ipLength);
        byteBuffer.put(mIP.getBytes());
        byteBuffer.putInt(mPort);
        mSerialized = byteBuffer.array();
    }

    @Override
    protected void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        byte messageType = byteBuffer.get();
        int usernameLength = byteBuffer.getInt();
        byte[] usernameBytes = new byte[usernameLength];
        byteBuffer.get(usernameBytes);
        mUsername = new String(usernameBytes);
        int ipLength = byteBuffer.getInt();
        byte[] ipBytes = new byte[ipLength];
        byteBuffer.get(ipBytes);
        mIP = new String(ipBytes);
        mPort = byteBuffer.getInt();
    }

    @Override
    public Byte getMassageType() {
        return MessageTypes.REQUEST_LOGIN_HOST;
    }


    public String getmUsername() {
        return mUsername;
    }

    public String getmIP() {
        return mIP;
    }

    public int getmPort() {
        return mPort;
    }
}
