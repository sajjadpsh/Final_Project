package ir.ac.aut.logic.network;

/**
 * Created by Sajjad on 7/2/2017.
 */
public abstract class BaseMassage {

    protected abstract void serialize();

    protected abstract void deserialize();


    protected byte[] mSerialized;

    public abstract Byte getMassageType();

    public byte[] getSerialized() {
        return mSerialized;
    }


}
