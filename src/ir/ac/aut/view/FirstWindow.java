package ir.ac.aut.view;

import ir.ac.aut.logic.network.MessageManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by Sajjad on 7/9/2017.
 */
public class FirstWindow extends JFrame {

    private JTextField name;
    private JRadioButton host;
    private JTextField hostPort;
    private JRadioButton guest;
    private JTextField ip;
    private JTextField guestPort;
    private JButton start;
    private JButton exit;
    private JLabel label;
    private JPanel panel;
    ButtonGroup buttonGroup;
    MessageManager mMessageManager;
    JTextArea mJtextArea;

    public FirstWindow() {
        super("Select Connection Mode");
        setLayout(new FlowLayout(FlowLayout.CENTER));
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        GridBagConstraints gbc = new GridBagConstraints();
        label = new JLabel();
        label.setText("Name ");
        add(panel);
        panel.add(label);
        name = new JTextField(25);
        add(name);
        panel.add(name);

        host = new JRadioButton();
        host.setText("Host");
        panel.add(host);

        label = new JLabel();
        label.setText("Port");
        panel.add(label);
        hostPort = new JTextField(15);
        panel.add(hostPort);

        guest = new JRadioButton();
        guest.setText("Guest");
        panel.add(guest);

        label = new JLabel();
        label.setText("IP");
        panel.add(label);
        ip = new JTextField(15);
        panel.add(ip);

        label = new JLabel();
        label.setText("Port");
        panel.add(label);
        guestPort = new JTextField(15);
        panel.add(guestPort);

        start = new JButton("Start");
        start.setBounds(12, 12, 12, 12);
        panel.add(start);
        exit = new JButton("Exit");
        exit.setBounds(12, 12, 12, 12);
        panel.add(exit);

        buttonGroup = new ButtonGroup();
        buttonGroup.add(host);
        buttonGroup.add(guest);

        action();
    }

    public void runGui() {
        FirstWindow firstWindow = new FirstWindow();
        firstWindow.setDefaultCloseOperation(EXIT_ON_CLOSE);
        firstWindow.setSize(625, 425);
        firstWindow.setVisible(true);
    }

    public void action() {

        Action action = new Action();
        start.addActionListener(action);

        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }

    private class Action implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == start) {
                String nameText = name.getText();
                if (host.isSelected()) {
                    String porthost = hostPort.getText();
                    new Thread() {
                        public void run() {
                            try {
                                mMessageManager = new MessageManager(Integer.parseInt(porthost), mJtextArea);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }.start();
                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException ex) {
                    }

                    try {
                        mMessageManager.sendRequestLoginHostMSG(nameText,Integer.parseInt(porthost));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    new WaitForConnection();
                } else if (guest.isSelected()) {
                    final String portC = guestPort.getText();
                    final String ipC = ip.getText();

                    new Thread() {
                        public void run() {
                            try {
                                mMessageManager = new MessageManager(portC, Integer.parseInt(ipC), mJtextArea);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }.start();

                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException ex) {
                    }
                    //
                    try {
                        mMessageManager.sendRequestLoginclientMSG(nameText, portC, ipC);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
    }

    public MessageManager getmMessageManager() {
        return mMessageManager;
    }
}
