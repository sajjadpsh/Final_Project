package ir.ac.aut.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GamePanel extends JFrame implements ActionListener, MouseListener {

    JButton[][] buttons = new JButton[10][10];
    private int[] remainingCound = new int[]{4, 3, 2, 1};
    private String Button = "*";

    GamePanel() {
        this.setTitle("Battleship");
        this.pack();
        this.setMaximumSize();
        this.setSize(1950, 1080);
        this.setLocation(0, 0);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JMenuBar menuBar = new JMenuBar();
        menuBar.setPreferredSize(new Dimension(300, 40));
        setJMenuBar(menuBar);
        JMenu file = new JMenu("    File  \uF031    ");
        file.setToolTipText("file");
        file.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 20));
        file.setHorizontalTextPosition(JLabel.RIGHT);
        menuBar.add(file);

        JMenuItem history;
        history = new JMenuItem("History     \uF032");
        history.setToolTipText("History");
        history.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 17));
        file.add(history);
        history.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.CTRL_MASK));

        JMenuItem exit;
        exit = new JMenuItem("Exit   \uF02E");
        exit.setToolTipText("Exit");
        exit.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 17));
        file.add(exit);
        exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));

        JMenu help = new JMenu("   Help  \uF022       ");
        help.setToolTipText("edit");
        help.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 20));
        menuBar.add(help);

        JPanel chatPanel = new JPanel();
        chatPanel.setBounds(1550, 50, 350, 860);
        chatPanel.setBackground(new Color(250, 255, 251));
        add(chatPanel);
        chatPanel.setLayout(new BorderLayout(0, 770));

        JTextArea chatArea = new JTextArea("~~~  Chat  To  Rival   ~~~~", 1, 33);
        chatArea.setEnabled(false);
        chatArea.setBackground(new Color(208, 49, 25));
        chatArea.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 30));
        chatPanel.add(chatArea, BorderLayout.NORTH);

        JTextArea spaceArea = new JTextArea("~~~~~  Chat    To    Rival     ~~~~~~", 1, 33);
        spaceArea.setEnabled(false);
        spaceArea.setBackground(new Color(208, 49, 25));
        spaceArea.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 20));
        chatPanel.add(spaceArea);

        JTextArea chatAreaTypeHere = new JTextArea("TypeHere...", 3, 22);
        chatAreaTypeHere.setLineWrap(true);
        chatAreaTypeHere.setWrapStyleWord(true);
        //chatAreaTypeHere.setBounds(1550, 1000, 345, 200);
        chatAreaTypeHere.setBackground(new Color(203, 208, 109));
        chatPanel.add(chatAreaTypeHere, BorderLayout.SOUTH);
        chatAreaTypeHere.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 25));
        JScrollPane chatAreaTypeHereScroll = new JScrollPane(chatAreaTypeHere, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        chatPanel.add(chatAreaTypeHereScroll);


        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                JButton buttons = new JButton();
//                buttons.setLocation(j * panelGuest.getWidth() / 10 + 850, i * panelGuest.getHeight() / 10 + 50);
//                buttons.setSize(panelGuest.getWidth() / 10, panelGuest.getHeight() / 10);
                buttons.setLocation(j * 600 / 10 + 850, i * 600 / 10 + 50);
                buttons.setSize(600 / 10, 600 / 10);
                buttons.setBackground(new Color(250, 255, 251));
                //buttons.setBorder(BorderFactory.createEtchedBorder(Color.BLACK,Color.DARK_GRAY));
                buttons.setBorderPainted(true);
                buttons.setContentAreaFilled(true);
                buttons.setRolloverEnabled(true);
                add(buttons);
            }
        }
        JPanel panelHost = new JPanel();
        panelHost.setBounds(850, 50, 600, 600);


        JPanel hostPanel = new JPanel();
        hostPanel.setBounds(850, 50, 600, 600);

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                int finalI = i;
                int finalJ = j;
                int finalJ1 = j;
                int finalI1 = i;

                buttons[i][j] = new JButton();
                buttons[i][j].setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 25));
                buttons[i][j].setLocation(j * panelHost.getWidth() / 10 + 100, i * panelHost.getHeight() / 10 + 50);
                buttons[i][j].setSize(panelHost.getWidth() / 10, panelHost.getHeight() / 10);
                //buttons[i][j].setLocation(j * 600 / 10 + 100, i * 600 / 10 + 50);
                //buttons[i][j].setSize(600 / 10,600 / 10);
                buttons[i][j].setBackground(new Color(250, 255, 251));
                buttons[i][j].setCursor(new Cursor(Cursor.HAND_CURSOR));
                buttons[i][j].addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent event) {
                        buttons[finalI][finalJ].setBackground(new Color(58, 58, 232));
                        buttons[finalI][finalJ].setText(String.valueOf(1));
                        String.valueOf(1);
                        System.out.println(String.valueOf(buttons[finalI][finalJ].getText()));
                    }
                });
                add(buttons[i][j]);
            }
        }
        setLayout(null);
        setVisible(true);

        JPanel conditionPanelGuest = new JPanel();
        conditionPanelGuest.setLayout(new GridLayout(4, 2, 10, 10));
        conditionPanelGuest.setBounds(100, 700, 600, 200);
        conditionPanelGuest.setBackground(new Color(252, 255, 124));
        add(conditionPanelGuest);


//        switch (Button){
//            case "aircraftCarrier":
//                for (int i = 0; i < 10; i++) {
//                    for (int j = 0; j < 10; j++) {
//                        int finalI = i;
//                        int finalJ = j;
//                        int finalJ1 = j;
//                        int finalI1 = i;
//
//                        //  int finalJ2 = j;
//                        aircraftCarrier.addMouseListener(new MouseAdapter() {
//                            @Override
//                            public void mousePressed(MouseEvent event) {
//                                buttons[finalI][finalJ].addMouseListener(new MouseAdapter() {
//                                    @Override
//                                    public void mousePressed(MouseEvent event) {
//                                        buttons[finalI][finalJ].setBackground(new Color(58, 58, 232));
//                                        buttons[finalI][finalJ].setText(String.valueOf(1));
//                                        String.valueOf(1);
//                                        System.out.println(String.valueOf(buttons[finalI][finalJ].getText()));
//
//
//                                    }
//                                });
//                            }
//
//                        });
//                    }
//                }
//                aircraftCarrier.disable();
//                break;
//
//            case "jkbkh":
//
//
//        }


        JButton auto = new JButton("Auto  ◙ ◙ ◙ ◙ ◙ ◙ ");
        conditionPanelGuest.add(auto);

        auto.setBackground(new Color(232, 89, 205));
        auto.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 20));
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                int finalI = i;
                int finalJ = j;
                int finalJ1 = j;
                int finalI1 = i;

                //  int finalJ2 = j;
                auto.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent event) {
                        buttons[finalI][finalJ].addMouseListener(new MouseAdapter() {
                            @Override
                            public void mousePressed(MouseEvent event) {
                                buttons[1][1].setText(String.valueOf(1));
                                buttons[1][1].setBackground(new Color(58, 58, 232));
                                buttons[1][4].setText(String.valueOf(1));
                                buttons[1][4].setBackground(new Color(58, 58, 232));
                                buttons[1][5].setText(String.valueOf(1));
                                buttons[1][5].setBackground(new Color(58, 58, 232));
                                buttons[1][7].setText(String.valueOf(1));
                                buttons[1][7].setBackground(new Color(58, 58, 232));
                                buttons[0][7].setText(String.valueOf(1));
                                buttons[0][7].setBackground(new Color(58, 58, 232));
                                buttons[2][7].setText(String.valueOf(1));
                                buttons[2][7].setBackground(new Color(58, 58, 232));
                                buttons[3][2].setText(String.valueOf(1));
                                buttons[3][2].setBackground(new Color(58, 58, 232));
                                buttons[3][3].setText(String.valueOf(1));
                                buttons[3][3].setBackground(new Color(58, 58, 232));
                                buttons[3][4].setText(String.valueOf(1));
                                buttons[3][4].setBackground(new Color(58, 58, 232));
                                buttons[5][3].setText(String.valueOf(1));
                                buttons[5][3].setBackground(new Color(58, 58, 232));
                                buttons[5][6].setText(String.valueOf(1));
                                buttons[5][6].setBackground(new Color(58, 58, 232));
                                buttons[5][8].setText(String.valueOf(1));
                                buttons[5][8].setBackground(new Color(58, 58, 232));
                                buttons[6][6].setText(String.valueOf(1));
                                buttons[6][6].setBackground(new Color(58, 58, 232));
                                buttons[6][8].setText(String.valueOf(1));
                                buttons[6][8].setBackground(new Color(58, 58, 232));
                                buttons[7][3].setText(String.valueOf(1));
                                buttons[7][3].setBackground(new Color(58, 58, 232));
                                buttons[7][6].setText(String.valueOf(1));
                                buttons[7][6].setBackground(new Color(58, 58, 232));
                                buttons[8][1].setText(String.valueOf(1));
                                buttons[8][1].setBackground(new Color(58, 58, 232));
                                buttons[8][6].setText(String.valueOf(1));
                                buttons[8][6].setBackground(new Color(58, 58, 232));
                                buttons[8][8].setText(String.valueOf(1));
                                buttons[8][8].setBackground(new Color(58, 58, 232));
                                buttons[9][1].setText(String.valueOf(1));
                                buttons[9][1].setBackground(new Color(58, 58, 232));
                                buttons[0][0].setText(String.valueOf(2));
                                buttons[0][0].setBackground(new Color(238, 221, 42));
                                buttons[0][1].setText(String.valueOf(2));
                                buttons[0][1].setBackground(new Color(238, 221, 42));
                                buttons[0][2].setText(String.valueOf(2));
                                buttons[0][2].setBackground(new Color(238, 221, 42));
                                buttons[0][3].setText(String.valueOf(2));
                                buttons[0][3].setBackground(new Color(238, 221, 42));
                                buttons[0][4].setText(String.valueOf(2));
                                buttons[0][4].setBackground(new Color(238, 221, 42));
                                buttons[0][5].setText(String.valueOf(2));
                                buttons[0][5].setBackground(new Color(238, 221, 42));
                                buttons[0][6].setText(String.valueOf(2));
                                buttons[0][6].setBackground(new Color(238, 221, 42));
                                buttons[0][8].setText(String.valueOf(2));
                                buttons[0][8].setBackground(new Color(238, 221, 42));
                                buttons[1][0].setText(String.valueOf(2));
                                buttons[1][0].setBackground(new Color(238, 221, 42));
                                buttons[1][2].setText(String.valueOf(2));
                                buttons[1][2].setBackground(new Color(238, 221, 42));
                                buttons[1][3].setText(String.valueOf(2));
                                buttons[1][3].setBackground(new Color(238, 221, 42));
                                buttons[1][6].setText(String.valueOf(2));
                                buttons[1][6].setBackground(new Color(238, 221, 42));
                                buttons[1][8].setText(String.valueOf(2));
                                buttons[1][8].setBackground(new Color(238, 221, 42));
                                buttons[2][0].setText(String.valueOf(2));
                                buttons[2][0].setBackground(new Color(238, 221, 42));
                                buttons[2][1].setText(String.valueOf(2));
                                buttons[2][1].setBackground(new Color(238, 221, 42));
                                buttons[2][2].setText(String.valueOf(2));
                                buttons[2][2].setBackground(new Color(238, 221, 42));
                                buttons[2][3].setText(String.valueOf(2));
                                buttons[2][3].setBackground(new Color(238, 221, 42));
                                buttons[2][4].setText(String.valueOf(2));
                                buttons[2][4].setBackground(new Color(238, 221, 42));
                                buttons[2][5].setText(String.valueOf(2));
                                buttons[2][5].setBackground(new Color(238, 221, 42));
                                buttons[2][6].setText(String.valueOf(2));
                                buttons[2][6].setBackground(new Color(238, 221, 42));
                                buttons[2][8].setText(String.valueOf(2));
                                buttons[2][8].setBackground(new Color(238, 221, 42));
                                buttons[3][1].setText(String.valueOf(2));
                                buttons[3][1].setBackground(new Color(238, 221, 42));
                                buttons[3][5].setText(String.valueOf(2));
                                buttons[3][5].setBackground(new Color(238, 221, 42));
                                buttons[3][6].setText(String.valueOf(2));
                                buttons[3][6].setBackground(new Color(238, 221, 42));
                                buttons[3][7].setText(String.valueOf(2));
                                buttons[3][7].setBackground(new Color(238, 221, 42));
                                buttons[3][8].setText(String.valueOf(2));
                                buttons[3][8].setBackground(new Color(238, 221, 42));
                                buttons[4][1].setText(String.valueOf(2));
                                buttons[4][1].setBackground(new Color(238, 221, 42));
                                buttons[4][2].setText(String.valueOf(2));
                                buttons[4][2].setBackground(new Color(238, 221, 42));
                                buttons[4][3].setText(String.valueOf(2));
                                buttons[4][3].setBackground(new Color(238, 221, 42));
                                buttons[4][4].setText(String.valueOf(2));
                                buttons[4][4].setBackground(new Color(238, 221, 42));
                                buttons[4][5].setText(String.valueOf(2));
                                buttons[4][5].setBackground(new Color(238, 221, 42));
                                buttons[4][6].setText(String.valueOf(2));
                                buttons[4][6].setBackground(new Color(238, 221, 42));
                                buttons[4][7].setText(String.valueOf(2));
                                buttons[4][7].setBackground(new Color(238, 221, 42));
                                buttons[4][8].setText(String.valueOf(2));
                                buttons[4][8].setBackground(new Color(238, 221, 42));
                                buttons[4][9].setText(String.valueOf(2));
                                buttons[4][9].setBackground(new Color(238, 221, 42));
                                buttons[5][2].setText(String.valueOf(2));
                                buttons[5][2].setBackground(new Color(238, 221, 42));
                                buttons[5][4].setText(String.valueOf(2));
                                buttons[5][4].setBackground(new Color(238, 221, 42));
                                buttons[5][5].setText(String.valueOf(2));
                                buttons[5][5].setBackground(new Color(238, 221, 42));
                                buttons[5][7].setText(String.valueOf(2));
                                buttons[5][7].setBackground(new Color(238, 221, 42));
                                buttons[5][9].setText(String.valueOf(2));
                                buttons[5][9].setBackground(new Color(238, 221, 42));
                                buttons[6][2].setText(String.valueOf(2));
                                buttons[6][2].setBackground(new Color(238, 221, 42));
                                buttons[6][3].setText(String.valueOf(2));
                                buttons[6][3].setBackground(new Color(238, 221, 42));
                                buttons[6][4].setText(String.valueOf(2));
                                buttons[6][4].setBackground(new Color(238, 221, 42));
                                buttons[6][5].setText(String.valueOf(2));
                                buttons[6][5].setBackground(new Color(238, 221, 42));
                                buttons[6][7].setText(String.valueOf(2));
                                buttons[6][7].setBackground(new Color(238, 221, 42));
                                buttons[6][9].setText(String.valueOf(2));
                                buttons[6][9].setBackground(new Color(238, 221, 42));
                                buttons[7][0].setText(String.valueOf(2));
                                buttons[7][0].setBackground(new Color(238, 221, 42));
                                buttons[7][1].setText(String.valueOf(2));
                                buttons[7][1].setBackground(new Color(238, 221, 42));
                                buttons[7][2].setText(String.valueOf(2));
                                buttons[7][2].setBackground(new Color(238, 221, 42));
                                buttons[7][4].setText(String.valueOf(2));
                                buttons[7][4].setBackground(new Color(238, 221, 42));
                                buttons[7][5].setText(String.valueOf(2));
                                buttons[7][5].setBackground(new Color(238, 221, 42));
                                buttons[7][7].setText(String.valueOf(2));
                                buttons[7][7].setBackground(new Color(238, 221, 42));
                                buttons[7][8].setText(String.valueOf(2));
                                buttons[7][8].setBackground(new Color(238, 221, 42));
                                buttons[7][9].setText(String.valueOf(2));
                                buttons[7][9].setBackground(new Color(238, 221, 42));
                                buttons[8][0].setText(String.valueOf(2));
                                buttons[8][0].setBackground(new Color(238, 221, 42));
                                buttons[8][2].setText(String.valueOf(2));
                                buttons[8][2].setBackground(new Color(238, 221, 42));
                                buttons[8][3].setText(String.valueOf(2));
                                buttons[8][3].setBackground(new Color(238, 221, 42));
                                buttons[8][4].setText(String.valueOf(2));
                                buttons[8][4].setBackground(new Color(238, 221, 42));
                                buttons[8][5].setText(String.valueOf(2));
                                buttons[8][5].setBackground(new Color(238, 221, 42));
                                buttons[8][7].setText(String.valueOf(2));
                                buttons[8][7].setBackground(new Color(238, 221, 42));
                                buttons[8][9].setText(String.valueOf(2));
                                buttons[8][9].setBackground(new Color(238, 221, 42));
                                buttons[9][0].setText(String.valueOf(2));
                                buttons[9][0].setBackground(new Color(238, 221, 42));
                                buttons[9][2].setText(String.valueOf(2));
                                buttons[9][2].setBackground(new Color(238, 221, 42));
                                buttons[9][5].setText(String.valueOf(2));
                                buttons[9][5].setBackground(new Color(238, 221, 42));
                                buttons[9][6].setText(String.valueOf(2));
                                buttons[9][6].setBackground(new Color(238, 221, 42));
                                buttons[9][7].setText(String.valueOf(2));
                                buttons[9][7].setBackground(new Color(238, 221, 42));
                                buttons[9][8].setText(String.valueOf(2));
                                buttons[9][8].setBackground(new Color(238, 221, 42));
                                buttons[9][9].setText(String.valueOf(2));
                                buttons[9][9].setBackground(new Color(238, 221, 42));
                            }
                        });
                    }
                });
            }
        }


//444444444444444444444444444444444444444444444444444444444444444444444444444444444444
        JButton verticalAircraftCarrier = new JButton("Vertical Aircraft carrier   ");
        conditionPanelGuest.add(verticalAircraftCarrier);
        verticalAircraftCarrier.setBackground(new Color(21, 189, 4));
        verticalAircraftCarrier.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 20));
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                int finalI = i;
                int finalJ = j;

                //  int finalJ2 = j;
                verticalAircraftCarrier.addMouseListener(new MouseAdapter() {

                    @Override
                    public void mouseClicked(MouseEvent event) {
                        buttons[finalI][finalJ].addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent event) {

                                buttons[finalI][finalJ].setBackground(new Color(58, 58, 232));
                                buttons[finalI][finalJ].setText(String.valueOf(1));
                                buttons[finalI + 1][finalJ].setBackground(new Color(58, 58, 232));
                                buttons[finalI + 1][finalJ].setText(String.valueOf(1));
                                buttons[finalI + 2][finalJ].setBackground(new Color(58, 58, 232));
                                buttons[finalI + 2][finalJ].setText(String.valueOf(1));
                                buttons[finalI + 3][finalJ].setBackground(new Color(58, 58, 232));
                                buttons[finalI + 3][finalJ].setText(String.valueOf(1));
                                buttons[finalI - 1][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI + 2][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 2][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI + 2][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 2][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI + 3][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 3][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI + 3][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 3][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI + 4][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 4][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI + 4][finalJ].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 4][finalJ].setText(String.valueOf(2));
                                buttons[finalI + 4][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 4][finalJ + 1].setText(String.valueOf(2));

                            }
                        });
                    }
                });
            }
        }


        JButton horizontalAircraftCarrier = new JButton("Horizontal Aircraft Carrier  ");
        conditionPanelGuest.add(horizontalAircraftCarrier);
        horizontalAircraftCarrier.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 20));
        horizontalAircraftCarrier.setBackground(new Color(21, 189, 4));
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                int finalI = i;
                int finalJ = j;
                int finalJ1 = j;
                int finalI1 = i;

                //  int finalJ2 = j;
                horizontalAircraftCarrier.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent event) {
                        buttons[finalI][finalJ].addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent event) {
                                buttons[finalI][finalJ].setBackground(new Color(58, 58, 232));
                                buttons[finalI][finalJ].setText(String.valueOf(1));
                                buttons[finalI][finalJ + 1].setBackground(new Color(58, 58, 232));
                                buttons[finalI][finalJ + 1].setText(String.valueOf(1));
                                buttons[finalI][finalJ + 2].setBackground(new Color(58, 58, 232));
                                buttons[finalI][finalJ + 2].setText(String.valueOf(1));
                                buttons[finalI][finalJ + 3].setBackground(new Color(58, 58, 232));
                                buttons[finalI][finalJ + 3].setText(String.valueOf(1));

                                buttons[finalI - 1][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ + 2].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ + 2].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ + 3].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ + 3].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ + 4].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ + 4].setText(String.valueOf(2));
                                buttons[finalI][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI][finalJ + 4].setBackground(new Color(238, 221, 42));
                                buttons[finalI][finalJ + 4].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ + 2].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ + 2].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ + 3].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ + 3].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ + 4].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ + 4].setText(String.valueOf(2));


                            }
                        });
                    }
                });
            }
        }


//333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
        JButton verticalCruisers = new JButton("Vertical Cruisers  ◙◙◙");
        conditionPanelGuest.add(verticalCruisers);
        verticalCruisers.setBackground(new Color(255, 252, 250));
        verticalCruisers.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 20));
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                int finalI = i;
                int finalJ = j;

                verticalCruisers.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent event) {
                        buttons[finalI][finalJ].addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent event) {
                                buttons[finalI][finalJ].setBackground(new Color(58, 58, 232));
                                buttons[finalI][finalJ].setText(String.valueOf(1));
                                buttons[finalI + 1][finalJ].setBackground(new Color(58, 58, 232));
                                buttons[finalI + 1][finalJ].setText(String.valueOf(1));
                                buttons[finalI + 2][finalJ].setBackground(new Color(58, 58, 232));
                                buttons[finalI + 2][finalJ].setText(String.valueOf(1));

                                buttons[finalI - 1][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI + 2][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 2][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI + 2][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 2][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI + 3][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 3][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI + 3][finalJ].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 3][finalJ].setText(String.valueOf(2));
                                buttons[finalI + 3][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 3][finalJ + 1].setText(String.valueOf(2));
//


                            }
                        });
                    }
                });

            }
        }


        JButton horizontalCruisers = new JButton("Horizontal Cruisers ◙◙◙");
        conditionPanelGuest.add(horizontalCruisers);
        horizontalCruisers.setBackground(new Color(255, 252, 250));
        horizontalCruisers.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 20));
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                int finalI = i;
                int finalJ = j;
                int finalJ1 = j;
                int finalI1 = i;

                //  int finalJ2 = j;
                int finalJ2 = j;
                int finalI2 = i;
                horizontalCruisers.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent event) {
                        buttons[finalI][finalJ].addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent event) {
                                buttons[finalI][finalJ].setBackground(new Color(58, 58, 232));
                                buttons[finalI][finalJ].setText(String.valueOf(1));
                                buttons[finalI][finalJ + 1].setBackground(new Color(58, 58, 232));
                                buttons[finalI][finalJ + 1].setText(String.valueOf(1));
                                buttons[finalI][finalJ + 2].setBackground(new Color(58, 58, 232));
                                buttons[finalI][finalJ + 2].setText(String.valueOf(1));

                                buttons[finalI - 1][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ + 2].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ + 2].setText(String.valueOf(2));
                                buttons[finalI - 1][finalJ + 3].setBackground(new Color(238, 221, 42));
                                buttons[finalI - 1][finalJ + 3].setText(String.valueOf(2));
                                buttons[finalI][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI][finalJ + 3].setBackground(new Color(238, 221, 42));
                                buttons[finalI][finalJ + 3].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ - 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ - 1].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ + 1].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ + 1].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ + 2].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ + 2].setText(String.valueOf(2));
                                buttons[finalI + 1][finalJ + 3].setBackground(new Color(238, 221, 42));
                                buttons[finalI + 1][finalJ + 3].setText(String.valueOf(2));
//

                            }
                        });
                    }
                });

            }
        }

//222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222
        JButton verticalSubmarine = new JButton("Vertical Submarine  ◙◙");
        conditionPanelGuest.add(verticalSubmarine);
        verticalSubmarine.setBackground(new Color(221, 39, 36));
        verticalSubmarine.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 20));
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                int finalI = i;
                int finalJ = j;
                int finalJ1 = j;
                int finalI1 = i;

                //  int finalJ2 = j;
                verticalSubmarine.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent event) {
                        buttons[finalI][finalJ].addActionListener(new verticalSubmarine(finalI, finalJ));///////////

                    }


//                    @Override
//                    public void mouseClicked(MouseEvent event) {
//
//                            buttons[finalI][finalJ].addMouseListener(new MouseAdapter() {
//
//                                @Override
//                                public void mouseClicked(MouseEvent event) {
//
//                                    buttons[finalI][finalJ].setBackground(new Color(58, 58, 232));
//                                    buttons[finalI][finalJ].setText(String.valueOf(1));
//                                    buttons[finalI + 1][finalJ].setBackground(new Color(58, 58, 232));
//                                    buttons[finalI + 1][finalJ].setText(String.valueOf(1));
//
//                                    buttons[finalI - 1][finalJ - 1].setBackground(new Color(238, 221, 42));
//                                    buttons[finalI - 1][finalJ - 1].setText(String.valueOf(2));
//                                    buttons[finalI - 1][finalJ].setBackground(new Color(238, 221, 42));
//                                    buttons[finalI - 1][finalJ].setText(String.valueOf(2));
//                                    buttons[finalI - 1][finalJ + 1].setBackground(new Color(238, 221, 42));
//                                    buttons[finalI - 1][finalJ + 1].setText(String.valueOf(2));
//                                    buttons[finalI][finalJ - 1].setBackground(new Color(238, 221, 42));
//                                    buttons[finalI][finalJ - 1].setText(String.valueOf(2));
//                                    buttons[finalI][finalJ + 1].setBackground(new Color(238, 221, 42));
//                                    buttons[finalI][finalJ + 1].setText(String.valueOf(2));
//                                    buttons[finalI + 1][finalJ - 1].setBackground(new Color(238, 221, 42));
//                                    buttons[finalI + 1][finalJ - 1].setText(String.valueOf(2));
//                                    buttons[finalI + 1][finalJ + 1].setBackground(new Color(238, 221, 42));
//                                    buttons[finalI + 1][finalJ + 1].setText(String.valueOf(2));
//                                    buttons[finalI + 2][finalJ].setBackground(new Color(238, 221, 42));
//                                    buttons[finalI + 2][finalJ].setText(String.valueOf(2));
//                                    buttons[finalI + 2][finalJ - 1].setBackground(new Color(238, 221, 42));
//                                    buttons[finalI + 2][finalJ - 1].setText(String.valueOf(2));
//                                    buttons[finalI + 2][finalJ + 1].setBackground(new Color(238, 221, 42));
//                                    buttons[finalI + 2][finalJ + 1].setText(String.valueOf(2));
////
//
//                                }
//                            });
//
//                    }
                });
            }
        }


        JButton horizontalSubmarine = new JButton("Horizontal Submarine  ◙◙");
        conditionPanelGuest.add(horizontalSubmarine);
        horizontalSubmarine.setBackground(new Color(221, 39, 36));
        horizontalSubmarine.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 20));
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                int finalI = i;
                int finalJ = j;
                //  int finalJ2 = j;
                horizontalSubmarine.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent event) {
                        buttons[finalI][finalJ].addActionListener(new horizontalSubmarine(finalI, finalJ));///////////

                    }
                });
            }
        }

//111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
        JButton gunboat = new JButton("Gunboat  ◙");
        gunboat.setFont(new Font("Monotone Corsica", Font.TYPE1_FONT + Font.CENTER_BASELINE, 20));
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                int finalI = i;
                int finalJ = j;
                int finalJ1 = j;
                int finalI1 = i;

                //  int finalJ2 = j;
                gunboat.addMouseListener(new MouseAdapter() {
                    @Override

                    public void mouseClicked(MouseEvent event) {
                        buttons[finalI][finalJ].addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent event) {

                                buttons[finalI][finalJ].setBackground(new Color(58, 58, 232));
                                buttons[finalI][finalJ].setText(String.valueOf(1));
                                String.valueOf(1);
                                buttons[finalI][finalJ].getText();
                                buttons[finalI][finalJ].disable();

                                if (!(buttons[finalI - 1][finalJ - 1].equals(String.valueOf(2)))) {
                                    buttons[finalI - 1][finalJ - 1].setBackground(new Color(238, 221, 42));
                                    buttons[finalI - 1][finalJ - 1].setText(String.valueOf(2));
                                    buttons[finalI - 1][finalJ - 1].getText();
                                    buttons[finalI - 1][finalJ - 1].disable();
                                }
                                if (!(buttons[finalI - 1][finalJ].equals(String.valueOf(2)))) {
                                    buttons[finalI - 1][finalJ].setBackground(new Color(238, 221, 42));
                                    buttons[finalI - 1][finalJ].setText(String.valueOf(2));
                                    buttons[finalI - 1][finalJ].getText();
                                    buttons[finalI - 1][finalJ].setEnabled(false);
                                }
                                if (!(buttons[finalI - 1][finalJ + 1].equals(String.valueOf(2)))) {
                                    buttons[finalI - 1][finalJ + 1].setBackground(new Color(238, 221, 42));
                                    buttons[finalI - 1][finalJ + 1].setText(String.valueOf(2));
                                    buttons[finalI - 1][finalJ + 1].getText();
                                    buttons[finalI - 1][finalJ + 1].setEnabled(false);
                                }
                                if (!(buttons[finalI][finalJ - 1].equals(String.valueOf(2)))) {
                                    buttons[finalI][finalJ - 1].setBackground(new Color(238, 221, 42));
                                    buttons[finalI][finalJ - 1].setText(String.valueOf(2));
                                    buttons[finalI][finalJ - 1].getText();
                                    buttons[finalI][finalJ - 1].setEnabled(false);
                                }
                                if (!(buttons[finalI][finalJ + 1].equals(String.valueOf(2)))) {
                                    buttons[finalI][finalJ + 1].setBackground(new Color(238, 221, 42));
                                    buttons[finalI][finalJ + 1].setText(String.valueOf(2));
                                    buttons[finalI][finalJ + 1].getText();
                                    buttons[finalI][finalJ + 1].setEnabled(false);
                                }
                                if (!(buttons[finalI + 1][finalJ - 1].equals(String.valueOf(2)))) {
                                    buttons[finalI + 1][finalJ - 1].setBackground(new Color(238, 221, 42));
                                    buttons[finalI + 1][finalJ - 1].setText(String.valueOf(2));
                                    buttons[finalI + 1][finalJ - 1].getText();
                                    buttons[finalI + 1][finalJ - 1].setEnabled(false);
                                }
                                if (!(buttons[finalI + 1][finalJ].equals(String.valueOf(2)))) {
                                    buttons[finalI + 1][finalJ].setBackground(new Color(238, 221, 42));
                                    buttons[finalI + 1][finalJ].setText(String.valueOf(2));
                                    buttons[finalI + 1][finalJ].getText();
                                    buttons[finalI + 1][finalJ].setEnabled(false);
                                }
                                if (!(buttons[finalI + 1][finalJ + 1].equals(String.valueOf(2)))) {
                                    buttons[finalI + 1][finalJ + 1].setBackground(new Color(238, 221, 42));
                                    buttons[finalI + 1][finalJ + 1].setText(String.valueOf(2));
                                    buttons[finalI + 1][finalJ + 1].getText();
                                    buttons[finalI + 1][finalJ + 1].setEnabled(false);
                                }


                            }
                        });
                    }
                });
            }
        }

        conditionPanelGuest.add(gunboat);
        gunboat.setBackground(new Color(232, 89, 205));

        JPanel conditionPanelHost = new JPanel();
        conditionPanelHost.setLayout(new GridLayout(4, 2, 10, 10));
        conditionPanelHost.setBounds(850, 700, 600, 200);
        conditionPanelHost.setBackground(new Color(238, 221, 42));
        add(conditionPanelHost);
        JButton aircraftCarrier1 = new JButton("Aircraft carrier  ◙ ◙ ◙ ◙ ");
        conditionPanelHost.add(aircraftCarrier1);
        JButton cruisers1 = new JButton("Cruisers  ◙ ◙ ◙");
        conditionPanelHost.add(cruisers1);
        JButton submarine1 = new JButton("Submarine  ◙ ◙");
        conditionPanelHost.add(submarine1);
        JButton gunboat1 = new JButton("Gunboat  ◙");
        conditionPanelHost.add(gunboat1);

        JPanel panelGuest = new JPanel();
        panelGuest.setBounds(1100, 50, 600, 600);
        this.revalidate();

        verticalAircraftCarrier.addActionListener((ActionEvent e) -> {
            horizontalAircraftCarrier.setEnabled(false);
        });
    }

    public static void main(String[] args) {
        GamePanel gamePanel = new GamePanel();
        gamePanel.setVisible(true);
        // new TextAreaExample();
    }

    private void setMaximumSize() {
    }
    //***********************************************************

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {


    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public class verticalSubmarine implements ActionListener {
        int finalI, finalJ;

        public verticalSubmarine(int x, int y) {
            this.finalI = x;
            this.finalJ = y;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            buttons[finalI][finalJ].setBackground(new Color(58, 58, 232));
            buttons[finalI][finalJ].setText(String.valueOf(1));
            buttons[finalI + 1][finalJ].setBackground(new Color(58, 58, 232));
            buttons[finalI + 1][finalJ].setText(String.valueOf(1));

            buttons[finalI - 1][finalJ - 1].setBackground(new Color(238, 221, 42));
            buttons[finalI - 1][finalJ - 1].setText(String.valueOf(2));
            buttons[finalI - 1][finalJ].setBackground(new Color(238, 221, 42));
            buttons[finalI - 1][finalJ].setText(String.valueOf(2));
            buttons[finalI - 1][finalJ + 1].setBackground(new Color(238, 221, 42));
            buttons[finalI - 1][finalJ + 1].setText(String.valueOf(2));
            buttons[finalI][finalJ - 1].setBackground(new Color(238, 221, 42));
            buttons[finalI][finalJ - 1].setText(String.valueOf(2));
            buttons[finalI][finalJ + 1].setBackground(new Color(238, 221, 42));
            buttons[finalI][finalJ + 1].setText(String.valueOf(2));
            buttons[finalI + 1][finalJ - 1].setBackground(new Color(238, 221, 42));
            buttons[finalI + 1][finalJ - 1].setText(String.valueOf(2));
            buttons[finalI + 1][finalJ + 1].setBackground(new Color(238, 221, 42));
            buttons[finalI + 1][finalJ + 1].setText(String.valueOf(2));
            buttons[finalI + 2][finalJ].setBackground(new Color(238, 221, 42));
            buttons[finalI + 2][finalJ].setText(String.valueOf(2));
            buttons[finalI + 2][finalJ - 1].setBackground(new Color(238, 221, 42));
            buttons[finalI + 2][finalJ - 1].setText(String.valueOf(2));
            buttons[finalI + 2][finalJ + 1].setBackground(new Color(238, 221, 42));
            buttons[finalI + 2][finalJ + 1].setText(String.valueOf(2));


        }
    }

    public class horizontalSubmarine implements ActionListener {
        int finalI, finalJ;

        public horizontalSubmarine(int x, int y) {
            this.finalI = x;
            this.finalJ = y;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            buttons[finalI][finalJ].setBackground(new Color(58, 58, 232));
            buttons[finalI][finalJ + 1].setBackground(new Color(58, 58, 232));
            buttons[finalI][finalJ].setText(String.valueOf(1));
            buttons[finalI][finalJ + 1].setText(String.valueOf(1));
            buttons[finalI - 1][finalJ - 1].setText(String.valueOf(2));
            buttons[finalI - 1][finalJ - 1].setBackground(new Color(238, 221, 42));
            buttons[finalI - 1][finalJ].setBackground(new Color(238, 221, 42));
            buttons[finalI - 1][finalJ].setText(String.valueOf(2));
            buttons[finalI - 1][finalJ + 1].setBackground(new Color(238, 221, 42));
            buttons[finalI - 1][finalJ + 1].setText(String.valueOf(2));
            buttons[finalI - 1][finalJ + 2].setBackground(new Color(238, 221, 42));
            buttons[finalI - 1][finalJ + 2].setText(String.valueOf(2));
            buttons[finalI][finalJ - 1].setBackground(new Color(238, 221, 42));
            buttons[finalI][finalJ - 1].setText(String.valueOf(2));
            buttons[finalI][finalJ + 2].setBackground(new Color(238, 221, 42));
            buttons[finalI][finalJ + 2].setText(String.valueOf(2));
            buttons[finalI + 1][finalJ - 1].setBackground(new Color(238, 221, 42));
            buttons[finalI + 1][finalJ - 1].setText(String.valueOf(2));
            buttons[finalI + 1][finalJ].setBackground(new Color(238, 221, 42));
            buttons[finalI + 1][finalJ].setText(String.valueOf(2));
            buttons[finalI + 1][finalJ].setBackground(new Color(238, 221, 42));
            buttons[finalI + 1][finalJ].setText(String.valueOf(2));
            buttons[finalI + 1][finalJ + 1].setBackground(new Color(238, 221, 42));
            buttons[finalI + 1][finalJ + 1].setText(String.valueOf(2));
            buttons[finalI + 1][finalJ + 2].setBackground(new Color(238, 221, 42));
            buttons[finalI + 1][finalJ + 2].setText(String.valueOf(2));
        }
    }
}
