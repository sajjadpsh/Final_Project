package ir.ac.aut.tools;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server extends Thread {
    private static ArrayList<BufferedWriter> clients;
    private String name;
    private Socket con;
    private BufferedReader bfr;

    private Server(Socket con) {
        this.con = con;
        try {
            InputStream in = con.getInputStream();
            InputStreamReader inr = new InputStreamReader(in);
            bfr = new BufferedReader(inr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            JLabel lblMessage = new JLabel("Server Port:" + "(Less than 65535)");
            lblMessage.setBackground(new Color(199, 208, 102));
            JTextField txtPort = new JTextField("");
            lblMessage.setBackground(new Color(146, 208, 148));
            Object[] texts = {lblMessage, txtPort};
            JOptionPane.showMessageDialog(null, texts);
            ServerSocket server = new ServerSocket(Integer.parseInt(txtPort.getText()));
            clients = new ArrayList<BufferedWriter>();
            JOptionPane.showMessageDialog(null, "Active Server at Port: " + txtPort.getText());

            while (true) {
                System.out.println("Wait for connect users\n");
                Socket con = server.accept();
                System.out.println("user Connected \n");
                Thread t = new Server(con);
                t.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }// End of of method main

    public void run() {
        try {
            String msg;
            OutputStream ou = this.con.getOutputStream();
            Writer ouw = new OutputStreamWriter(ou);
            BufferedWriter bfw = new BufferedWriter(ouw);
            clients.add(bfw);
            name = msg = bfr.readLine();
            while (!"Logout".equalsIgnoreCase(msg) && msg != null) {
                msg = bfr.readLine();
                sendToAll(bfw, msg);
                //System.out.println(msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendToAll(BufferedWriter bwOutput, String msg) throws IOException {
        BufferedWriter bwS;
        for (BufferedWriter bw : clients) {
            bwS = (BufferedWriter) bw;
            if (!(bwOutput == bwS)) {
                bw.write(name + " ---> " + msg + "\r\n");
                bw.flush();
            }
        }
    }

}