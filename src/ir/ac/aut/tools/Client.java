package ir.ac.aut.tools;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.net.Socket;

public class Client extends JFrame implements ActionListener, KeyListener {

    private Socket socket;
    private OutputStream outputStream;
    private Writer ouw;
    private BufferedWriter bfw;

    private JTextField txtIP;
    private JTextField txtPort;
    private JTextField txtName;
    private JTextArea text;
    private JTextField txtMsg;
    private JButton btnSend;
    private JButton btnLogout;

    public Client() throws IOException {
        JLabel lblMessage = new JLabel("Check!");
        txtIP = new JTextField("127.0.0.1");
        txtPort = new JTextField("");
        txtName = new JTextField("Client");
        Object[] texts = {lblMessage, txtIP, txtPort, txtName};
        JOptionPane.showMessageDialog(null, texts);
        JPanel pnlContent = new JPanel();
        text = new JTextArea(30, 30);

        text.setEditable(false);
        text.setBackground(new Color(150, 230, 150));
        txtMsg = new JTextField(30);
        txtMsg.setBackground(new Color(250, 250, 0));
        JLabel lblHistorico = new JLabel("History Talks  \uF034");
        JLabel lblMsg = new JLabel("Write a Message  \uF037");
        btnSend = new JButton("                              Send Message \uF02E                               ");
        btnSend.setBackground(Color.BLUE);
        // btnSend.setSize(800, 200);
        btnSend.setToolTipText("Send Message");
        btnLogout = new JButton("                               Logout of Chat  \uF054                               ");
        btnLogout.setBackground(Color.RED);
        btnLogout.setToolTipText("Logout of Chat");
        btnSend.addActionListener(this);
        btnLogout.addActionListener(this);
        btnSend.addKeyListener(this);
        txtMsg.addKeyListener(this);
        JScrollPane scroll = new JScrollPane(text);
        scroll.setBackground(Color.RED);
        text.setLineWrap(true);
        pnlContent.add(lblHistorico);
        pnlContent.add(scroll);
        pnlContent.add(lblMsg);
        pnlContent.add(txtMsg);
        pnlContent.add(btnLogout);
        pnlContent.add(btnSend);
        pnlContent.setBackground(new Color(185, 208, 207));
        text.setBorder(BorderFactory.createEtchedBorder(Color.BLACK, Color.BLACK));
        txtMsg.setBorder(BorderFactory.createEtchedBorder(Color.BLACK, Color.BLACK));
        setTitle(txtName.getText());
        setContentPane(pnlContent);
        setLocationRelativeTo(null);
        setResizable(false);
        setSize(350, 675);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) throws IOException {
        Client app = new Client();
        app.connect();
        app.listen();
    }

    public JTextField getTxtIP() {
        return txtIP;
    }

    public JTextField getTxtName() {
        return txtName;
    }

    public JTextArea getText() {
        return text;
    }

    public JTextField getTxtMsg() {
        return txtMsg;
    }

    private void connect() throws IOException {
        socket = new Socket(txtIP.getText(), Integer.parseInt(txtPort.getText()));
        outputStream = socket.getOutputStream();
        ouw = new OutputStreamWriter(outputStream);
        bfw = new BufferedWriter(ouw);
        bfw.write(txtName.getText() + "\r\n");
        bfw.flush();
    }

    public String getMsg1() {
        return msg1;
    }

    String msg1;
    public void sendMessage(String msg) throws IOException {
        if (msg.equals("Logout")) {
            bfw.write("Disconnected \r\n");
            text.append("Disconnected \r\n");
        } else {
            bfw.write(msg + "\r\n");
            msg1=msg;
            text.append(txtName.getText() + " ---> " + txtMsg.getText() + "\r\n");
        }
        bfw.flush();
        txtMsg.setText("");
    }

    private void listen() throws IOException {
        InputStream in = socket.getInputStream();
        InputStreamReader inr = new InputStreamReader(in);
        BufferedReader bfr = new BufferedReader(inr);
        String msg = "";
        while (!"Logout".equalsIgnoreCase(msg)) if (bfr.ready()) {
            msg = bfr.readLine();
            if (msg.equals("Logout")) text.append("Server out! \r\n");
            else text.append(msg + "\r\n");
        }
    }

    private void sair() throws IOException {
        sendMessage("Logout");
        bfw.close();
        ouw.close();
        outputStream.close();
        socket.close();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getActionCommand().equals(btnSend.getActionCommand())) sendMessage(txtMsg.getText());
            else if (e.getActionCommand().equals(btnLogout.getActionCommand())) sair();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                sendMessage(txtMsg.getText());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent arg0) {

    }

    @Override
    public void keyTyped(KeyEvent arg0) {

    }


}
