package ir.ac.aut.model;

import ir.ac.aut.tools.Client;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.swing.*;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Sajjad on 7/9/2017.
 */
public class JsonFile {
    public static void main(String[] args) throws IOException {

        JSONObject object = new JSONObject();
        Client client = new Client();
        object.put(client.getTxtName().getText(), client.getTxtIP().getText());

        JSONArray array = new JSONArray();

        array.put(client.getMsg1().toString());

        try (
                FileWriter file = new FileWriter("test.json")) {

            file.write(String.valueOf(object));
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print(object);

    }
}
