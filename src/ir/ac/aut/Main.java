package ir.ac.aut;


import ir.ac.aut.view.FirstWindow;

/**
 * Created by Sajjad on 7/3/2017.
 */
public class Main {

    public static void main(String[] args) {
        new FirstWindow().runGui();
    }
}
